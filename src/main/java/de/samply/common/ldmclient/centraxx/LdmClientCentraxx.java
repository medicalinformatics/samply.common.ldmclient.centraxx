/*
 * Copyright (c) 2017 Medical Informatics Group (MIG),
 * Universitätsklinikum Frankfurt
 *
 * Contact: www.mig-frankfurt.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.ldmclient.centraxx;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import de.samply.common.ldmclient.LdmClient;
import de.samply.common.ldmclient.LdmClientException;
import de.samply.common.ldmclient.LdmClientUtil;
import de.samply.common.ldmclient.centraxx.model.CentraxxInfo;
import de.samply.common.ldmclient.model.QueryResultPageKey;
import de.samply.share.model.query.common.Error;
import de.samply.share.model.ccp.QueryResult;
import de.samply.share.model.query.common.View;
import de.samply.share.model.queryresult.common.QueryResultStatistic;
import de.samply.share.utils.QueryConverter;
import org.apache.http.*;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.util.concurrent.ExecutionException;

/**
 * Client to communicate with the local datamanagement implementation "Centraxx"
 */
public class LdmClientCentraxx extends LdmClient<QueryResult> {

    final Logger logger = LoggerFactory.getLogger(LdmClientCentraxx.class);

    /** Unclassified error with attached stacktrace*/
    public static final int ERROR_CODE_UNCLASSIFIED_WITH_STACKTRACE = 1000;

    /** Function is not adequately implemented*/
    public static final int ERROR_CODE_UNIMPLEMENTED = 1001;

    /** Failed to parse a date value*/
    public static final int ERROR_CODE_DATE_PARSING_ERROR = 1002;

    /** One or more MDR Keys were not known. List of unknown keys is attached.*/
    public static final int ERROR_CODE_UNKNOWN_MDRKEYS = 1003;

    static final String REST_PATH_TEILER = "rest/teiler/";
    private static final String REST_PATH_INFO = "rest/info/";
    static final String REST_PATH_REQUESTS = "requests";
    private static final String REST_PATH_RESULT = "result";
    private static final String REST_PATH_STATS = "stats";
    private static final String REST_RESULTS_ONLY_SUFFIX = "?statisticsOnly=true";
    private static final String REST_PARAM_PAGE = "?page=";
    public static final boolean CACHING_DEFAULT_VALUE = false;
    public static final int CACHE_DEFAULT_SIZE = 1000;

    private boolean useCaching;
    private final int cacheSize;

    public LdmClientCentraxx(CloseableHttpClient httpClient, String centraxxBaseUrl) throws LdmClientException {
        this(httpClient, centraxxBaseUrl, CACHING_DEFAULT_VALUE);
    }

    public LdmClientCentraxx(CloseableHttpClient httpClient, String centraxxBaseUrl, boolean useCaching) throws LdmClientException {
        this(httpClient, centraxxBaseUrl, useCaching, CACHE_DEFAULT_SIZE);
    }

    public LdmClientCentraxx(CloseableHttpClient httpClient, String centraxxBaseUrl, boolean useCaching, int cacheSize) throws LdmClientException {
        super(httpClient, centraxxBaseUrl);
        this.useCaching = useCaching;
        this.cacheSize = cacheSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String postView(View view) throws LdmClientCentraxxException {
        return postView(view, false);
    }

    public String postView(View view, boolean statisticsOnly) throws LdmClientCentraxxException {

        if (view == null) {
            throw new LdmClientCentraxxException("View is null.");
        }

        String path = REST_PATH_REQUESTS;
        String viewString;
        try {
            de.samply.share.model.ccp.View ccpView = QueryConverter.convertCommonViewToCcpView(view);
            viewString = QueryConverter.marshal(ccpView, JAXBContext.newInstance(de.samply.share.model.ccp.View.class));
        } catch (JAXBException e) {
            throw new LdmClientCentraxxException(e);
        }
        String location;
        if (statisticsOnly) {
            path = path + REST_RESULTS_ONLY_SUFFIX;
        }

        HttpPost httpPost = new HttpPost(LdmClientUtil.addTrailingSlash(getLdmBaseUrl()) + REST_PATH_TEILER + path);
        HttpEntity entity = new StringEntity(viewString, Consts.UTF_8);
        httpPost.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_XML.getMimeType());
        httpPost.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_XML.getMimeType());
        httpPost.setEntity(entity);

        CloseableHttpResponse response;
        int statusCode;
        try {
            response = getHttpClient().execute(httpPost);
            statusCode = response.getStatusLine().getStatusCode();
            location = response.getFirstHeader(HttpHeaders.LOCATION).getValue();
            response.close();
        } catch (IOException e) {
            throw new LdmClientCentraxxException(e);
        }

        if (statusCode != HttpStatus.SC_CREATED) {
            logger.error("Request not created. Received status code " + statusCode);
            throw new LdmClientCentraxxException("Request not created. Received status code " + statusCode);
        } else if (LdmClientUtil.isNullOrEmpty(location)) {
            throw new LdmClientCentraxxException("Empty location received");
        }

        return location;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String postViewString(String viewString) throws LdmClientCentraxxException {
        return postViewString(viewString, false);
    }

    public String postViewString(String viewString, boolean statisticsOnly) throws LdmClientCentraxxException {
        View view;
        try {
            view = QueryConverter.xmlToView(viewString);
        } catch (JAXBException e) {
            logger.debug("JAXB Exception while trying to convert string to common view. Trying different namespace...");
            view = null;
        }

        if (view == null) {
            try {
                de.samply.share.model.ccp.View ccpView = QueryConverter.unmarshal(viewString,
                        JAXBContext.newInstance(de.samply.share.model.ccp.ObjectFactory.class),
                        de.samply.share.model.ccp.View.class);
                view = QueryConverter.convertCcpViewToCommonView(ccpView);
            } catch (JAXBException e) {
                throw new LdmClientCentraxxException(e);
            }
        }

        return postView(view, statisticsOnly);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryResult getResult(String location) throws LdmClientCentraxxException {
        QueryResult queryResult = new QueryResult();
        QueryResultStatistic queryResultStatistic = getQueryResultStatistic(location);

        if (queryResultStatistic != null && queryResultStatistic.getTotalSize() > 0) {
            for (int i = 0; i < queryResultStatistic.getNumberOfPages(); ++i) {
                QueryResult queryResultPage = getResultPage(location, i);
                queryResult.getPatient().addAll(queryResultPage.getPatient());
            }
            queryResult.setId(queryResultStatistic.getRequestId());
        }
        return queryResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryResult getResultPage(String location, int page) throws LdmClientCentraxxException, IndexOutOfBoundsException {
        if (useCaching) {
            try {
                return QueryResultsCacheManager.getCache(this).get(new QueryResultPageKey(location, page));
            } catch (ExecutionException e) {
                logger.warn("Error when trying to use cache. Querying Centraxx directly.", e);
                return getResultPageFromCentraxx(location, page);
            }
        } else {
            return getResultPageFromCentraxx(location, page);
        }
    }

    /**
     * Get a single page of a query result from centraxx
     * @param location the location of the result, as retrieved from centraxx via the POST of the request
     * @param page the page index
     * @return the partial query result
     */
    public QueryResult getResultPageFromCentraxx(String location, int page) throws LdmClientCentraxxException, IndexOutOfBoundsException {
        if (page < 0) {
            throw new IndexOutOfBoundsException("Requested page index < 0: " + page);
        }
        Object statsOrError = getStatsOrError(location);

        if (statsOrError.getClass().equals(QueryResultStatistic.class)) {
            QueryResultStatistic queryResultStatistic = (QueryResultStatistic) statsOrError;
            if (queryResultStatistic.getNumberOfPages() < (page + 1) ) { // pages start at 0
                throw new IndexOutOfBoundsException("Requested page: " + page + " , number of pages is " + queryResultStatistic.getNumberOfPages());
            }
        } else {
            throw new LdmClientCentraxxException("No QueryResultStatistics found at stats location.");
        }

        HttpGet httpGet = new HttpGet(LdmClientUtil.addTrailingSlash(location) + REST_PATH_RESULT + REST_PARAM_PAGE + page);
        // Apparently, it may take a bit longer to reply when a new user session has to be created...so use an extensive timeout of 1 minute
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000).setConnectionRequestTimeout(60000).build();
        httpGet.setConfig(requestConfig);

        try {
            CloseableHttpResponse response = getHttpClient().execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            String queryResultString = EntityUtils.toString(entity, Consts.UTF_8);
            EntityUtils.consume(entity);
            if (HttpStatus.SC_OK == statusCode) {
                return QueryConverter.ccpXmlToQueryResult(queryResultString);
            } else {
                throw new LdmClientCentraxxException("While trying to get Result page " + page + " statuscode " + statusCode + " was received from Centraxx");
            }
        } catch (IOException | JAXBException e) {
            throw new LdmClientCentraxxException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getStatsOrError(String location) throws LdmClientCentraxxException {
        HttpGet httpGet = new HttpGet(LdmClientUtil.addTrailingSlash(location) + REST_PATH_STATS);
        // Apparently, it may take a bit longer to reply when a new user session has to be created...so use an extensive timeout of 1 minute
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000).setConnectionRequestTimeout(60000).build();
        httpGet.setConfig(requestConfig);

        try {
            CloseableHttpResponse response = getHttpClient().execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            HttpEntity entity = response.getEntity();
            String entityOutput = EntityUtils.toString(entity, Consts.UTF_8);
            if (statusCode == HttpStatus.SC_OK) {
                JAXBContext jaxbContext = JAXBContext.newInstance(de.samply.common.ldmclient.centraxx.model.QueryResultStatistic.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                de.samply.common.ldmclient.centraxx.model.QueryResultStatistic qrs = (de.samply.common.ldmclient.centraxx.model.QueryResultStatistic) jaxbUnmarshaller.unmarshal(new StringReader(entityOutput));
                EntityUtils.consume(entity);
                response.close();
                return convertQueryResultStatisticToCommonQueryResultStatistic(qrs);
            } else if (statusCode == HttpStatus.SC_UNPROCESSABLE_ENTITY) {
                JAXBContext jaxbContext = JAXBContext.newInstance(de.samply.share.model.ccp.Error.class);
                Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                de.samply.share.model.ccp.Error error = ((de.samply.share.model.ccp.Error) jaxbUnmarshaller.unmarshal(new StringReader(entityOutput)));
                EntityUtils.consume(entity);
                response.close();
                return QueryConverter.convertCcpErrorToCommonError(error);
            } else if (statusCode == HttpStatus.SC_ACCEPTED) {
                response.close();
                logger.debug("Statistics not written yet. Centraxx is probably busy with another request.");
                return null;
            } else {
                response.close();
                throw new LdmClientCentraxxException("Unexpected response code: " + statusCode);
            }
        } catch (IOException | JAXBException e) {
            throw new LdmClientCentraxxException("While trying to read stats/error", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QueryResultStatistic getQueryResultStatistic(String location) throws LdmClientCentraxxException {
        Object statsOrError = getStatsOrError(location);

        if (statsOrError == null) {
            throw new LdmClientCentraxxException("Stats not readable");
        } else if (statsOrError.getClass().equals(QueryResultStatistic.class)) {
            return (QueryResultStatistic)statsOrError;
        } else {
            throw new LdmClientCentraxxException("Stats not available. Get error file instead.");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Error getError(String location) throws LdmClientCentraxxException {
        Object statsOrError = getStatsOrError(location);

        if (statsOrError == null) {
            throw new LdmClientCentraxxException("Error not readable");
        } else if (statsOrError.getClass().equals(Error.class)) {
            return (Error)statsOrError;
        } else {
            throw new LdmClientCentraxxException("Error not available. Try to get query result statistics file instead.");
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getResultCount(String location) throws LdmClientCentraxxException {
        Integer totalSize = null;
        QueryResultStatistic queryResultStatistic = getQueryResultStatistic(location);
        if (queryResultStatistic != null) {
            totalSize = queryResultStatistic.getTotalSize();
        }
        return totalSize;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String getVersionString() throws LdmClientCentraxxException {
        CentraxxInfo centraxxInfo = getInfo();
        return centraxxInfo.getCentraxxVersion();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUserAgentInfo() throws LdmClientException {
        return "Centraxx/" + getVersionString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isResultPageAvailable(String location, int pageIndex) {
        if (pageIndex < 0) {
            return false;
        }

        HttpHead httpHead = new HttpHead(LdmClientUtil.addTrailingSlash(location) + REST_PATH_RESULT + REST_PARAM_PAGE + pageIndex);
        // Apparently, it may take a bit longer to reply when a new user session has to be created...so use an extensive timeout of 1 minute
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(60000).setConnectTimeout(60000).setConnectionRequestTimeout(60000).build();
        httpHead.setConfig(requestConfig);

        try {
            CloseableHttpResponse response = getHttpClient().execute(httpHead);
            int statusCode = response.getStatusLine().getStatusCode();
            return HttpStatus.SC_OK == statusCode;
        } catch (IOException e) {
            return false;
        }
    }

    private CentraxxInfo getInfo() throws LdmClientCentraxxException {
        ResponseHandler<CentraxxInfo> responseHandler = new ResponseHandler<CentraxxInfo>() {

            @Override
            public CentraxxInfo handleResponse(final HttpResponse response) throws IOException {
                StatusLine statusLine = response.getStatusLine();
                HttpEntity entity = response.getEntity();
                if (statusLine.getStatusCode() >= 300) {
                    throw new HttpResponseException(
                            statusLine.getStatusCode(),
                            statusLine.getReasonPhrase());
                }
                if (entity == null) {
                    throw new ClientProtocolException("Response contains no content");
                }
                InputStream instream = entity.getContent();
                try (Reader reader = new InputStreamReader(instream, Consts.UTF_8)) {
                    return new Gson().fromJson(reader, CentraxxInfo.class);
                } catch (JsonSyntaxException | JsonIOException e) {
                    logger.warn("JSON Exception caught while trying to unmarshal centraxx info...");
                    return null;
                }  finally {
                    instream.close();
                }
            }
        };

        String infoUrl = LdmClientUtil.addTrailingSlash(getLdmBaseUrl()) + REST_PATH_INFO;
        try {
            return getHttpClient().execute(new HttpGet(infoUrl), responseHandler);
        } catch (IOException e) {
            throw new LdmClientCentraxxException("Could not read centraxx info", e);
        }
    }

    public int getCacheSize() {
        return cacheSize;
    }

    public void cleanQueryResultsCache() {
        QueryResultsCacheManager.cleanCache(this);
    }

    /**
     * Transforms a CCP QueryResultStatistic to a common QueryResultStatistic
     *
     * @param queryResultStatistic the QueryResultStatistic in the centraxx namespace
     * @return the QueryResultStatistic in the common namespace
     */
    public static QueryResultStatistic convertQueryResultStatisticToCommonQueryResultStatistic(de.samply.common.ldmclient.centraxx.model.QueryResultStatistic queryResultStatistic) {
        QueryResultStatistic commonQueryResultStatistic = new QueryResultStatistic();
        commonQueryResultStatistic.setNumberOfPages(queryResultStatistic.getNumberOfPages());
        commonQueryResultStatistic.setRequestId(queryResultStatistic.getRequestId());
        commonQueryResultStatistic.setTotalSize(queryResultStatistic.getTotalSize());
        return commonQueryResultStatistic;
    }
}
