# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.0.1] - 2017-12-15
### Fixed
- Wrong class was used to check if it is a stats file or error file

## [3.0.0] - 2017-12-15
### Changed
- Adapt to v3(.0.1) of the interface ldmclient

## [2.0.0] - UNRELEASED
### Changed
- Adapt to v2 of the interface ldmclient

## [1.1.2] - 2017-10-09
### Changed
- Use QueryConverter from share.dto

### Fixed
- Check if cache is null before trying to invalitate it

## [1.1.1] - 2017-09-13
### Added
- Cache QueryResults (optional)

## [1.1.0] - Unreleased
- Skipped due to error in parent class

## [1.0.1] - 2017-04-27
### Added
- Get User Agent Info (ldm name + version info)

### Changed
- Increased timeouts when reading stats/error or results

## [1.0.0] - skipped
### Added
- First implementation

